import { useState, useEffect, useContext } from 'react';
import { Form, Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {

	const { user } = useContext(UserContext);

	// an object with methods to redirect the user
	const navigate = useNavigate();

	const { productId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(1);

	const checkout = (productId) => {
		fetch(`https://cpstn2-ecommerceapi-agtang.onrender.com/orders/checkout`, {
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				products: [
		      		{
		        		productId: productId,
		        		quantity: quantity
		        	}
		        ]
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data.message = 'Checked Out'){
				Swal.fire({
					title: "Successfully checked out",
					icon: 'success',
					text: "You have successfully checked out the product."
				})
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: 'error',
					text: "Please try again."
				})
			}
		})
	}

	useEffect(() => {
		console.log(productId);

		fetch(`https://cpstn2-ecommerceapi-agtang.onrender.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description)
			setPrice(data.price);
		})
	}, [productId])

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3}}>
					<Card.Body className="text-center">
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price</Card.Subtitle>
						<Card.Text>₱{price.toFixed(2)}</Card.Text>
						<Row>
							<Card.Subtitle>Quantity:</Card.Subtitle>
						</Row>
								<Col className="justify-content-center d-flex">
									<Form.Group>
										<Form.Control
											className="mt-2 mb-3"
											type="number"
											value={quantity}
											onChange={(e) => setQuantity(e.target.value)}
											min="1"
											style={{ width: '70px' }}
										/>
									</Form.Group>
								</Col>
						
						{ user.id !== null ?
							<Button variant="dark" block onClick={() => checkout(productId)}>Add to Orders</Button>
							:
							<Link className="btn btn-danger btn-block" to="/login">Log in to buy</Link>
						}
												
					</Card.Body>
				</Col>
			</Row>
		</Container>
	)
}