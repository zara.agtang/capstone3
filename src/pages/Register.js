import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function Register(){

	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	// State hooks to store the values of the input fields
	const [ firstName, setFirstName ] = useState("");
	const [lastName,setLastName] = useState("");
    const [email,setEmail] = useState("");
    const [mobileNo,setMobileNo] = useState("");
    const [password,setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");	

	//State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    // Checking
    console.log(firstName);
    console.log(lastName);
    console.log(email);
    console.log(mobileNo);
    console.log(password);
    console.log(confirmPassword);

    // Fetch

    function registerUser(e){
    	//prevent page redirection via form submission
    	e.preventDefault();

    	fetch('https://cpstn2-ecommerceapi-agtang.onrender.com/users/register',{
    		method: 'POST',
    		headers:{
    			"Content-Type":"application/json"
    		},
    		body: JSON.stringify({

    			firstName: firstName,
    			lastName: lastName,
    			email: email,
    			mobileNo: mobileNo,
    			password: password

    		})
    	})
    	.then(res=>res.json())
    	.then(data=>{

    		console.log(data)
    		if(data){

    			setFirstName('');
    			setLastName('');
    			setEmail('');
    			setMobileNo('');
    			setPassword('');
    			setConfirmPassword('')

    			Swal.fire({
    				title: "Successful registration!",
    				icon: "success",
    				text: "Welcome to SoundZations! You may now login to your account."
    			})

    			navigate("/login"); // Navigate to login page

    		}else{
    			Swal.fire({
    				title: "Oops!",
    				icon: "error",
    				text: "Please try again."
    			})
    		}
    	})
    }


	useEffect(()=>{

    	if((firstName !== "" && lastName !== "" && email !=="" && mobileNo !== "" && password !=="" && confirmPassword !== "") && (password === confirmPassword) && (mobileNo.length === 11)){

    		setIsActive(true)

    	}else{
    		setIsActive(false)
    	}

    },[firstName,lastName,email,mobileNo,password,confirmPassword])



	return(
		(user.id !== null) ?
			<Navigate to="/login" />
		:
		<Form onSubmit={(e)=>registerUser(e)}>
			<h1 className="my-2 text-center">Sign Up</h1>

			<Form.Group>
				<Form.Control
					className="container p-3 my-2 d-flex flex-column w-50 mb-3"
					type="text"
					placeholder="First Name"
					required
					value={firstName}
					onChange={e=>{setFirstName(e.target.value)}}
				/>
			</Form.Group>

			<Form.Group>
				<Form.Control
					className="container p-3 my-2 d-flex flex-column w-50 mb-3"
					type="text"
					placeholder="Last Name"
					required
					value={lastName}
					onChange={e=>{setLastName(e.target.value)}}
				/>
			</Form.Group>

			<Form.Group>
				<Form.Control
					className="container p-3 my-2 d-flex flex-column w-50 mb-3"
					type="email"
					placeholder="Email address"
					required
					value={email}
					onChange={e=>{setEmail(e.target.value)}}
				/>
			</Form.Group>

			<Form.Group>
				<Form.Control
					className="container p-3 my-2 d-flex flex-column w-50 mb-3"
					type="text"
					placeholder="11-digit Mobile No."
					required
					value={mobileNo}
					onChange={e=>{setMobileNo(e.target.value)}}
				/>
			</Form.Group>

			<Form.Group>
				<Form.Control
					className="container p-3 my-2 d-flex flex-column w-50 mb-3"
					type="password"
					placeholder="Password"
					required
					value={password}
					onChange={e=>{setPassword(e.target.value)}}
				/>
			</Form.Group>

			<Form.Group>
				<Form.Control
					className="container p-3 my-2 d-flex flex-column w-50 mb-3"
					type="password"
					placeholder="Confirm Password"
					required
					value={confirmPassword}
					onChange={e=>{setConfirmPassword(e.target.value)}}
				/>
			</Form.Group>

			<Form.Group className="my-4 text-center">
			{
				isActive ? (
				<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
			)	: 	(
				<Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
			)}
			</Form.Group>

		</Form>
	)
}
