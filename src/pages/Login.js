import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login({userId, fetchData}) {

    const { user, setUser } = useContext(UserContext);

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);

    function authenticate(e) {

    // Prevents page redirection via form submission
    e.preventDefault();
    fetch('https://cpstn2-ecommerceapi-agtang.onrender.com/users/login',{

    method: 'POST',
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({

        email: email,
        password: password

    })
})
.then(res => res.json())
.then(data => {


    if(typeof data.access !== "undefined"){

        localStorage.setItem('token', data.access);
        console.log('Token stored:', data.access); // Verify if the authentication token is correctly stored in local storage when the user logs in

        // function for retrieving details
        retrieveUserDetails(data.access);

        setUser({
            access: localStorage.getItem('token')
        })

        Swal.fire({
            title: "Login successful",
            icon: "success",
            text: "Welcome to SoundZations!"
        })
        
    } else {

        Swal.fire({
            title: "Authentication failed",
            icon: "error",
            text: "Check your login details and try again"
        })
    }
})
// Clear input fields after submission
setEmail('');
setPassword('');


}

    const retrieveUserDetails = (token) => {
        fetch(`https://cpstn2-ecommerceapi-agtang.onrender.com/users/details`, {
            headers: {
                Authorization: `Bearer ${ token } `
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
            if(email !== '' && password !== ''){
                setIsActive(true);
            }else{
                setIsActive(false);
            }

        }, [email, password]);

return (    
        (user.id !== null) ?
            <Navigate to="/products" />
        :
        <Form onSubmit={(e)=>authenticate(e)}>
            <h1 className="my-5 text-center">Login</h1>
            <Form.Group controlId="userEmail">
                
                <Form.Control
                    className="container p-3 my-2 d-flex flex-column w-50 mb-3" 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                
                <Form.Control
                    className="container p-3 my-2 d-flex flex-column w-50" 
                    type="password" 
                    placeholder="Enter Password" 
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group className="my-4 text-center">
            { isActive ? (
                <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
            )    :     (
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            )}
            </Form.Group>
        </Form>       
)
}
