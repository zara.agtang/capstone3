import { useContext, useState, useEffect } from 'react';
import { Row, Col, Table, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import '../App.css';
import Swal from 'sweetalert2';

export default function Profile() {
    const { user } = useContext(UserContext);

    const [details, setDetails] = useState({});
    const [products, setProducts] = useState([]);
    const [orders, setOrders] = useState([]);

    useEffect(() => {
        fetch(`https://cpstn2-ecommerceapi-agtang.onrender.com/users/details`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
        })
            .then((res) => res.json())
            .then((data) => {
            console.log(data);

            if (typeof data.id !== undefined) {
                setDetails(data);
            }
        });
    }, []);

    useEffect(() => {
        // Fetch the user's orders
        fetch(`https://cpstn2-ecommerceapi-agtang.onrender.com/orders/myOrders`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
        })
            .then((res) => res.json())
            .then((data) => {
            console.log(data);

            if (Array.isArray(data)) {
                setOrders(data);
            }
        });
    }, []);


    // Delete Order
    const handleDeleteOrder = (orderId) => {

        fetch(`https://cpstn2-ecommerceapi-agtang.onrender.com/orders/${orderId}`, {
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
        })
        .then((res) => {
            if (res.status === 200) {
                // Remove the deleted order from the state
                setOrders((prevOrders) => prevOrders.filter((order) => order._id !== orderId));
                Swal.fire({

                    icon:"success",
                    title: "Product has been deleted"

                })
            } else {
                console.error('Failed to delete order');
                Swal.fire({

                    icon: "error",
                    title: "Unsuccessful product deletion"

                })
            }
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    };

    return details.id === null ? (
        <Navigate to="/products" />
    ) : (
        <>
            <Row>
                <Col className="text-dark text-center">
                    <h1 className="ms-auto">{`${details.firstName} ${details.lastName}`}</h1>
                    <h6>Email: {`${details.email}`}</h6>
                    <h6>Mobile No: {`${details.mobileNo}`}</h6>
                    <hr />
                </Col>
            </Row>
            <div>
                <Row>
                    <Col>
                        <h3>My Orders</h3>
                        <Table striped bordered hover responsive>
                            <thead>
                                <tr className="text-center">
                                    <th>Order ID</th>
                                    <th>Product</th>
                                    <th>Quantity</th>
                                    <th>Product Price</th>
                                    <th>Sub-total</th>
                                </tr>
                            </thead>
                            <tbody className="text-center">
                                {orders.map((order) => (
                                    <tr key={order._id}>
                                        <td>{order._id}</td>
                                        <td>
                                            {order.products.map((product) => (
                                                <div key={product._id}>
                                                    {product.productName}
                                                </div>
                                            ))}
                                        </td>
                                        <td>
                                            {order.products.map((product) => (
                                                <div key={product._id}>
                                                    {product.quantity}
                                                </div>
                                            ))}
                                        </td>
                                        <td>
                                            {order.products.map((product) => (
                                                <div key={product._id}>
                                                    ₱{product.productPrice.toFixed(2)}
                                                </div>
                                            ))}
                                        </td>
                                        <td>
                                            {order.products.map((product) => (
                                                <div key={product._id}>
                                                    ₱{(product.quantity * product.productPrice).toFixed(2)}
                                                </div>
                                            ))}
                                        </td>
                                        <td>
                                            <Button variant="danger" onClick={() => handleDeleteOrder(order._id)}>Delete</Button>
                                        </td>
                                    </tr>
                                ))}
                                <tr>
                                    <td colSpan="4" className="text-right">
                                        <strong>Total Amount</strong>
                                    </td>
                                    <td>
                                    <strong>
                                        ₱{orders.reduce((total, order) => {
                                            return total + order.products.reduce((subTotal, product) => {
                                                return subTotal + product.quantity * product.productPrice;
                                            }, 0);
                                        }, 0).toFixed(2)}
                                    </strong>
                                    </td>
                                </tr>
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </div>
        </>
    );
}