import Banner from '../components/Banner';
// import FeaturedProducts from '../components/FeaturedProducts';
import Highlights from '../components/Highlights';

export default function Home() {

const data = {
    title: "SoundZations",
    content: "Harmony Awaits: Your Melody, Your Instruments!",
    destination: "/products",
    label: "Buy now!"
}

	return (
		<>
			<Banner data={data} />
			<Highlights />
		</>
		
	)

}