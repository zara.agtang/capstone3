import '../App.css';
import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {

	return (
  		<Row className="mt-3 mb-3">
      		<Col xs={12} md={4}>
        		<Card className="cardHighlight p-3">
          			<div className="aspect-ratio">
			            <Card.Img
			              	className="card-image img-fluid"
				            variant="top"
				            src="https://news.inverhills.edu/wp-content/uploads/2012/11/concert-music-musical-instrument-violin-1170x680.jpg"
				            alt="A girl playing violin"
			            />
          			</div>
          			<Card.Body>
            			<Card.Title>
              				<h2>Unlock Your Creative Potential</h2>
            			</Card.Title>
			            <Card.Text>
			              Investing in a musical instrument can unlock your creative potential and open up a world of artistic expression. Whether you're a beginner or an experienced musician, having the right instrument at your disposal allows you to explore your musical talents and develop your skills. From composing your own music to playing your favorite songs, a musical instrument is the gateway to your creative journey.
			            </Card.Text>
          			</Card.Body>
        		</Card>
      		</Col>
      		<Col xs={12} md={4}>
        		<Card className="cardHighlight p-3">
          			<div className="aspect-ratio">
            			<Card.Img
              				className="card-image img-fluid"
              				variant="top"
              				src="https://beatsure.com/wp-content/uploads/2020/01/Learning-drums-at-40-e1580057036204.jpg"
              				alt="Group of people playing instruments"
            			/>
          			</div>
	          		<Card.Body>
	            		<Card.Title>
	              			<h2>Lifelong Learning and Skill Development</h2>
	            		</Card.Title>
			            <Card.Text>
			              Learning to play a musical instrument is a lifelong endeavor that offers numerous benefits. It promotes discipline, patience, and dedication while enhancing cognitive skills such as memory, problem-solving, and multitasking. Owning your own instrument ensures that you can practice and improve your skills at your own pace, leading to a sense of accomplishment and personal growth.
			            </Card.Text>
	          		</Card.Body>
        		</Card>
      		</Col>
      		<Col xs={12} md={4}>
        		<Card className="cardHighlight p-3">
          			<div className="aspect-ratio">
            			<Card.Img
				            className="card-image img-fluid"
				            variant="top"
				            src="https://www.americanacma.org/wp-content/uploads/2017/04/homepage-hero.jpg"
				            alt="Group of people playing instruments"
            			/>
          			</div>
          			<Card.Body>
            			<Card.Title>
              				<h2>Connect with Others and Join the Music Community</h2>
            			</Card.Title>
			            <Card.Text>
			              Music has a unique power to bring people together. When you own a musical instrument, you have the opportunity to connect with others who share your passion for music. Whether it's jamming with friends, joining a local band, or participating in community music events, owning an instrument facilitates social interactions and fosters a sense of belonging within the vibrant music community. So, don't miss out on the chance to be part of something greater – buy your musical instrument today and start making beautiful music with others.
			            </Card.Text>
          			</Card.Body>
        		</Card>
      		</Col>
    	</Row>
  	)
}
