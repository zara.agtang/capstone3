import { useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';

import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav'
import UserContext from '../UserContext';

export default function AppNavbar() {

		const { user } = useContext(UserContext);

		console.log(user); // Check if we receive the token

	return (
		<Navbar bg="dark" data-bs-theme="dark" expand="lg" sticky="top" className="shadow p-2 mb-5">
			<Container fluid>
				
				<Navbar.Brand as={Link} to="/" className="brand">
					<style>
						@import url('https://fonts.googleapis.com/css2?family=Rubik+Dirt&display=swap');
					</style>
					SoundZations
				</Navbar.Brand>

				<Navbar.Toggle aria-controls="basic-navbar-nav"/>
				<Navbar.Collapse id='basic-navbar-nav'>
					<Nav className="ms-auto">
					   <Nav.Link as={Link} to="/">Home</Nav.Link>
					   <Nav.Link as={Link} to="/products">Products</Nav.Link>
						   {(user.id !== null) ? 

						           user.isAdmin 
						           ?
						           <>
						           		<Nav.Link as={Link} to="/addProduct">Add Product</Nav.Link>
						               	<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
						           </>
						           :
						           <>
						           		<Nav.Link as={Link} to="/profile">Profile</Nav.Link>
						               	<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
						           </>
						       		: 
						           <>
						               	<Nav.Link as={Link} to="/login">Login</Nav.Link>
						               	<Nav.Link as={Link} to="/register">Register</Nav.Link>
						           </>
						   }
					   </Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	)
}