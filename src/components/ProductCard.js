import { useState } from 'react';
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
 
export default function ProductCard({productProp}) {

	const {_id, name, description, price, quantity} = productProp;

	console.log(useState(0))

	return (
		<Card className="mb-3">
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>₱{price.toFixed(2)}</Card.Text>
				<Link className="btn btn-dark" to={`/products/${_id}`}>Details</Link>
			</Card.Body>
		</Card>
	)
}

// Check if the CourseCard component is getting the correct prop types
ProductCard.propTypes = {
	product: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}