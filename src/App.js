import { useState, useEffect } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import Container from 'react-bootstrap/Container'

import AppNavbar from './components/AppNavbar'

import Register from './pages/Register'
import Login from './pages/Login';
import Logout from './pages/Logout';
import Home from './pages/Home';
import Products from './pages/Products';
import Profile from './pages/Profile';
import ProductView from './pages/ProductView';
import AddProduct from './components/AddProduct';

import './App.css';
import { UserProvider } from './UserContext';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  // Check if the user information is properly stored upon login
  useEffect(() => {

    fetch(`https://cpstn2-ecommerceapi-agtang.onrender.com/users/details`,{
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      console.log("App.js")
      // Set the user state values with the user details upon successful login.
      if (typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }
      else{
        setUser({
          id: null,
          isAdmin: null
        })
      } 
    })
  }, [user])

  return (
    
    <UserProvider value={{ user, setUser, unsetUser}}>
      <Router>
        
        <Container fluid>

          <AppNavbar />

          <Routes>

              <Route path="/" element={<Home/>} />
              <Route path="/products" element={<Products/>} />
              <Route exact path="/products/:productId" element={<ProductView/>} />
              <Route path="/register" element={<Register/>} />
              <Route path="/login" element={<Login/>} />
              <Route path="/logout" element={<Logout/>} />
              <Route path="/addProduct" element={<AddProduct/>} />
              <Route path="/profile" element={<Profile/>} />

          </Routes>
        </Container>
      </Router>
    </UserProvider>
    
  );
}

export default App;